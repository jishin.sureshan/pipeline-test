# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Students(models.Model):
    first_name = models.CharField(max_length=200, blank=True, null=True)
    last_name = models.CharField(max_length=200, blank=True, null=True)
    roll_no = models.IntegerField(default=0, blank=True, null=True)

    def __str__(self):
        return u"%s %s" % (self.first_name, self.last_name)


class MeetingType(models.Model):
    name = models.CharField(max_length=200, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    highlight_text = models.CharField(max_length=200, null=True, blank=True)
    order = models.PositiveIntegerField(default=0, null=True, blank=True)

    def __str__(self):
        return u"%s" % self.name
