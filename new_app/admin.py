# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from reversion.admin import VersionAdmin
from .models import MeetingType, Students


# Register your models here.

class MeetingTypeAdmin(admin.ModelAdmin):
    model = MeetingType


class StudentAdmin(admin.ModelAdmin):
    model = Students


admin.site.register(MeetingType, MeetingTypeAdmin)
admin.site.register(Students, StudentAdmin)
